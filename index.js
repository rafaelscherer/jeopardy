const axios = require('axios');
const fs = require('fs');

// Make a request for a user with a given ID
axios.get('https://jservice.kenzie.academy/api/clues')
    .then(function (response) {
        // let json = response.data.clues;
        let json = JSON.stringify(response.data.clues);
        fs.writeFileSync('./dicas.json', json);
        // console.log(json);
        // handle success
        // console.log(response);
    })
    .catch(function (error) {
        // handle error
        console.log(error);
    })
    .finally(function () {
        // always executed
    });